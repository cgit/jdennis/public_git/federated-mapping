package com.redhat.app;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.redhat.IdpMapping.InvalidTypeException;
import com.redhat.IdpMapping.IdpJson;
import com.redhat.IdpMapping.RuleProcessor;
import com.redhat.IdpMapping.InvalidRuleException;
import com.redhat.IdpMapping.UndefinedValueException;

class MappingApp {
  private static final Logger log = Logger.getLogger(MappingApp.class);


  private static String loadFile(String path) throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, "UTF-8");
  }

  public static void main(String[] args) {
    BasicConfigurator.configure();
    log.info("Federated Mapping");

    String rulesFilename = args[0];
    String assertionFilename = "assertion-01.json";
    String assertionJson;
    Map<String, Object> mapped;
    IdpJson json = new IdpJson();
    RuleProcessor rp = null;

    try {
      rp = new RuleProcessor(Paths.get(rulesFilename), null); // FIXME, mappings parameter
      assertionJson = loadFile(assertionFilename);

    } catch (IOException e) {
      log.error(e);
      return;
    }
    try {
      mapped = rp.process(assertionJson);
      System.out.println(String.format("mapped=%s", mapped));
      if (mapped != null) {
        System.out.println(json.dumpJson(mapped));
      }
    } catch (InvalidRuleException | UndefinedValueException | InvalidTypeException e) {
      // e.printStackTrace();
      log.error(e);
    }
  }
}
