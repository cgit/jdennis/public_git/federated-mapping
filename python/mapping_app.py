#!/usr/bin/python

import json
import logging
from idp_mapping import RuleProcessor
import sys
import traceback

LOG = logging.getLogger()
logging.basicConfig(level=logging.INFO, format='%(message)s')

rule_filename = 'rules-python-01.json'
assertion_filename = 'assertion-01.json'

def assertion_from_file(filename):
    with open(filename) as stream:
        assertion = json.load(stream)
    return assertion

def assertion_from_string(string):
    assertion = json.loads(string)
    return assertion

def main():
    if True:
        rule_processor = RuleProcessor.from_file(rule_filename)

    if False:
        with open(rule_filename) as stream:
            rule_processor = RuleProcessor.from_stream(stream)

    if False:
        with open(rule_filename) as stream:
            string = stream.read()
        rule_processor = RuleProcessor.from_string(string)

    assertion = assertion_from_file(assertion_filename)

    try:
        mapped = rule_processor.process(assertion)
        if mapped is None:
            print "no rules matched"
        else:
            for k, v in mapped.iteritems():
                print "%s: %s" % (k, v)
            mapped_json = json.dumps(mapped, indent=4)
            print "\nmapped JSON"
            print mapped_json
    except Exception as exc:
        print "FAIL: %s" % exc
        traceback.print_exc()
        sys.exit(1)
    sys.exit(0)

main()
